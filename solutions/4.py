''' Find the largest palindrome made from the product of two 3-digit numbers '''

def is_palindrome(n):
    return str(n) == str(n)[::-1]


def largest_palindrome_product(n):
    ''' get the largest palindrome created by multiplying
        two n-digit ints '''
    start = int("1" * n)
    end = int("9" * n)

    largest = 0

    for i in range(start, end + 1):
        for j in range(start, end + 1):
            p = i * j

            if is_palindrome(p) and p > largest:
                largest = p
    
    return largest


def main():
    print(largest_palindrome_product(3))

if __name__ == '__main__':
    main()