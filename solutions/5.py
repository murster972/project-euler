''' What is the smallest positive number that is evenly divisible by all of the numbers from 1 to 20?  '''


def smallest_number(n):
    ''' return smallest number that is divisible by all numbers from 1 to n '''
    number = n

    while True:
        # 1 is true for all numbers (bar 0) and we know n
        # to be true since we're increasing number by n
        # each time, so we only have to check between
        # 2 to n - 1
        error = False

        for i in range(2, n):
            if number % i != 0:
                error = True
                break

        if not error:
            return number

        # number must be divisible by n, hence we can increase by n
        # each time since we know number must be a factor of n
        number += n     


def main():
    s = smallest_number(20)
    print(s)

if __name__ == '__main__':
    main()