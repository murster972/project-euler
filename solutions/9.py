''' 
A Pythagorean triplet is a set of three natural numbers, a < b < c, for which,

a**2 + b**2 = c**2
For example, 3**2 + 4**2 = 9 + 16 = 25 = 5**2

There exists exactly one Pythagorean triplet for which a + b + c = 1000.
Find the product abc.
'''

import math

def pyth_trips(n):
    ''' finds the pyth triplets a, b, c for which a + b + c = n '''
    for a in range(1, n):
        for b in range(1, n):
            c = a**2 + b**2
            c **= 0.5

            if (a + b + c) == n and c.is_integer():
                return a, b, c


def main():
    n = 1000
    trips = pyth_trips(n)

    print(trips, math.prod(trips))

if __name__ == '__main__':
    main()