''' Find the difference between the sum of the squares of the first one
    hundred natural numbers and the square of the sum. '''

def sum_of_square(n):
    ''' return sum of the square for numbers 1 to n (inclusive) '''
    return sum(i**2 for i in range(1, n + 1))

def square_of_sum(n):
    ''' return square of sum for numbers 1 to n (inclusive) '''
    return sum(range(1, n + 1))**2    

def main():
    n = 100
    s1 = sum_of_square(n)
    s2 = square_of_sum(n)

    print(s2 - s1)

if __name__ == '__main__':
    main()