
''' By considering the terms in the Fibonacci sequence whose values do not exceed four
    million, find the sum of the even-valued terms. '''


def fib_numbers(limit: int):
    ''' generate fibonacci number upto (exclusive) the specific limit

        :param limit: max number
        :return: iterable object of fib numbers upto the limit '''
    fib_zero = 0
    fib_one = 1

    # generate and return subsequent values
    while fib_one < limit:
        yield fib_one

        # fn = fn-1 = fn-2, e.g. f = fib_one + fib_zero
        tmp = fib_one
        fib_one += fib_zero
        fib_zero = tmp


def main():
    limit = 4000000
    even_sum = sum(i for i in fib_numbers(limit) if i % 2 == 0)

    print(even_sum)

if __name__ == '__main__':
    main()