from math import prod


''' What is the largest prime factor of the number 600851475143 ? '''



def get_factors(n):
    ''' factor n into two numbers, or return
        None if we cannot factor n any further '''
    for i in range(2, n):
        if n % i == 0:
            return [i, n // i]

    return []

def gen_prime_factors(n):
    ''' work out the prime factors of n, will return an
        empty list if n is a prime number '''
    # see "Factor Tree" at following link
    # https://www.mathsisfun.com/prime-factorization.html
    factors = get_factors(n)
    prime_factors = []

    while factors:
        tmp = []

        for i in factors:
            i_factors = get_factors(i)

            if i_factors:
                tmp += i_factors
            else:
                prime_factors.append(i)
        
        factors = tmp

    return prime_factors

def main():
    n = 600851475143

    prime_factors = gen_prime_factors(n)

    print(prime_factors)
    
    print(prod(prime_factors), prod(prime_factors) == n)


    print(max(prime_factors))

if __name__ == '__main__':
    main()