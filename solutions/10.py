''' Find the sum of all the primes below two million. '''


def prime_generator(n):
    ''' generator all primes below n (exclusive) using
        a prime sieve '''
    # NOTE: this is 3rd or 4th iteration, have tried
    #       various implemntation of just a list
    #       and deleting items or creating new list
    #       without factors instead of marking
    #       them, however the over head of either
    #       having to find the index of an item
    #       or having to iterare the entire list
    #       each time negates any benefit of reducing
    #       the size of the list.
    numbers = [{0: i, 1: False} for i in range(2, n)]
    ind = 0
    n_len = len(numbers)

    while True:
        # find the next unmarked number
        try:
            while numbers[ind][1]:
                ind += 1

            cur = numbers[ind][0]
        except IndexError:
            break

        # mark each number that is a factor of the current
        # number
        for val in range(cur * 2, numbers[-1][0] + 1, cur):
            # index is -2 since our numbers list starts at
            # 2 and not zero
            val_ind = val - 2
    
            numbers[val_ind][1] = True

        # increment the current index
        ind += 1

    # return all unmarked numbers
    return [i[0] for i in numbers if not i[1]]

def main():
    n = 2000000
    primes = prime_generator(n)

    print(sum(primes))

if __name__ == '__main__':
    main()