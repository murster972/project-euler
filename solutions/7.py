import math


''' What is the 10 001st prime number? '''


def prime_counter(n):
    ''' returns the number of primes below n '''
    return int(n / math.log(n))

def prime_sieve(n):
    ''' generates a list of primes below n using the sieve
        of Eratosthenes '''
    numbers = list(range(2, n + 1))

    ind = 0

    while True:
        try:
            p = numbers[ind]
        except IndexError:
            # cheap way of checking if we have exceded the
            # numbers list without having to check the last
            # item or the length each time
            break

        # remove all numbers divisble by p
        numbers = [i for i in numbers if i % p != 0 or i == p]
        ind += 1

    return numbers


def generate_prime(n):
    ''' returns the nth prime number '''
    # calculate l, the number which has n primes
    # before it
    l = n

    while prime_counter(l) < n:
        l += 1

    prime_sieve(l)

    # generate primes upto l and return the nth
    # prime number
    return prime_sieve(l)[n - 1]

def main():
    n = 10001
    nth_prime = generate_prime(n)

    print(nth_prime)


if __name__ == '__main__':
    main()